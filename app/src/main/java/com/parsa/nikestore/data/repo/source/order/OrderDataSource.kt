package com.parsa.nikestore.data.repo.source.order

import com.parsa.nikestore.data.Checkout
import com.parsa.nikestore.data.SubmitOrderResult
import io.reactivex.Single

interface OrderDataSource {

    fun submit(
        firstName:String,
        lastName:String,
        postalCode:String,
        mobile:String,
        address:String,
        paymentMethod:String
    ):Single<SubmitOrderResult>

    fun checkOut(orderId:Int):Single<Checkout>
}