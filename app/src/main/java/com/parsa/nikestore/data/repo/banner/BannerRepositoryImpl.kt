package com.parsa.nikestore.data.repo.banner

import com.parsa.nikestore.data.Banner
import com.parsa.nikestore.data.repo.source.banner.BannerDataSoure
import io.reactivex.Single

class BannerRepositoryImpl(val bannerDataSoure: BannerDataSoure): BannerRepository {
    override fun getAll(): Single<List<Banner>> {
        return bannerDataSoure.getAll()
    }
}