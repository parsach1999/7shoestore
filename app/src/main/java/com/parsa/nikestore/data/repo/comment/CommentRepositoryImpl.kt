package com.parsa.nikestore.data.repo.comment

import com.parsa.nikestore.data.Comment
import com.parsa.nikestore.data.repo.source.comment.CommentDataSource
import io.reactivex.Single

class CommentRepositoryImpl(val commentDataSource: CommentDataSource): CommentRepository {
    override fun getAll(productId: Int): Single<List<Comment>> {
        return commentDataSource.getAll(productId)
    }

    override fun insert(): Single<Comment> {
        TODO("Not yet implemented")
    }
}