package com.parsa.nikestore.data.repo.source.comment

import com.parsa.nikestore.data.Comment
import io.reactivex.Single

interface CommentDataSource {

    fun getAll(productId:Int):Single<List<Comment>>

    fun insert():Single<Comment>
}