package com.parsa.nikestore.data

object TokenContainer {

    var token:String?=null
        private set
    var refreshToken:String?=null
        private set

    fun update(token:String?,refreshToken:String?){
        this.refreshToken=refreshToken
        this.token=token
    }
}