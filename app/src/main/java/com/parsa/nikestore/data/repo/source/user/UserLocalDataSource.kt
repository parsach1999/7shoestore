package com.parsa.nikestore.data.repo.source.user

import android.content.SharedPreferences
import com.parsa.nikestore.data.MessageResponse
import com.parsa.nikestore.data.TokenContainer
import com.parsa.nikestore.data.TokenResponse
import io.reactivex.Single


const val ACCESS_TOKEN="accessToken"
const val REFRESH_TOKEN="refreshToken"
class UserLocalDataSource(val sharedPreference:SharedPreferences): UserDataSource {
    override fun login(userName: String, password: String): Single<TokenResponse> {
        TODO("Not yet implemented")
    }

    override fun signUp(userName: String, password: String): Single<MessageResponse> {
        TODO("Not yet implemented")
    }

    override fun loadToken() {
        TokenContainer.update(sharedPreference.getString(ACCESS_TOKEN,null),
            sharedPreference.getString(REFRESH_TOKEN,null))
    }

    override fun saveToken(token: String, refresh: String) {
        sharedPreference.edit().apply {
            putString(ACCESS_TOKEN,token)
            putString(REFRESH_TOKEN,refresh)
        }.apply()
    }

    override fun saveUserName(userName: String) {
        sharedPreference.edit().apply {
            putString("username",userName)
        }.apply()
    }

    override fun getUserName(): String {
        return sharedPreference.getString("username","")?:""
    }

    override fun signOut() {
        sharedPreference.edit().apply {
            clear()
        }.apply()
    }
}
