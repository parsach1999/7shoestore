package com.parsa.nikestore.data

data class CartItemCount(
    var count: Int
)