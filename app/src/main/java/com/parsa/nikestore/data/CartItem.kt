package com.parsa.nikestore.data

data class CartItem(
    val cart_item_id: Int,
    var count: Int,
    val product: Product,
    var progressBarVisibility:Boolean=false
)