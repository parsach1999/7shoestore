package com.parsa.nikestore.data.repo.source.cart

import com.google.gson.JsonObject
import com.parsa.nikestore.data.AddToCartResponse
import com.parsa.nikestore.data.CartItemCount
import com.parsa.nikestore.data.CartResponse
import com.parsa.nikestore.data.MessageResponse
import com.parsa.nikestore.services.http.ApiService
import io.reactivex.Single

class CartRemoteDataSource(val apiService: ApiService): CartDataSource {
    override fun addToCart(productId: Int): Single<AddToCartResponse> {
        return apiService.addToCart(JsonObject().apply {
            addProperty("product_id",productId)
        })
    }

    override fun get(): Single<CartResponse> {
        return apiService.getCart()
    }
    override fun remove(cartItemId: Int): Single<MessageResponse> {
        return apiService.removeItemFromCart(JsonObject().apply {
            addProperty("cart_item_id",cartItemId)
        })
    }

    override fun changeCount(cartItemId: Int, count: Int): Single<AddToCartResponse> {
        return apiService.changeCount(JsonObject().apply {
            addProperty("cart_item_id",cartItemId)
            addProperty("count",count)
        })
    }

    override fun getCartItemCount(): Single<CartItemCount> {
        return apiService.getCartItemCount()
    }
}