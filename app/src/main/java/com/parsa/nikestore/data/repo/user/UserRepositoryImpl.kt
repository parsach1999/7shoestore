package com.parsa.nikestore.data.repo.user

import com.parsa.nikestore.data.TokenContainer
import com.parsa.nikestore.data.TokenResponse
import com.parsa.nikestore.data.repo.source.user.UserDataSource
import io.reactivex.Completable

class UserRepositoryImpl(val userRemoteDataSource: UserDataSource,
                         val userLocalDataSource: UserDataSource
): UserRepository {
    override fun login(userName: String, password: String): Completable {
        return userRemoteDataSource.login(userName,password).doOnSuccess {
            onSuccessfulLogin(userName,it)
        }.ignoreElement()
    }

    override fun signUp(userName: String, password: String): Completable {
        return userRemoteDataSource.signUp(userName,password).flatMap {
            userRemoteDataSource.login(userName,password)
        }.doOnSuccess {
            onSuccessfulLogin(userName,it)
        }.ignoreElement()
    }

    override fun loadToken() {
        userLocalDataSource.loadToken()
    }


    override fun getUserName(): String {
        return userLocalDataSource.getUserName()
    }

    override fun signOut() {
        userLocalDataSource.signOut()
        TokenContainer.update(null,null)
    }

    fun onSuccessfulLogin(userName: String,tokenResponse: TokenResponse){
        TokenContainer.update(tokenResponse.access_token,tokenResponse.refresh_token)
        userLocalDataSource.saveToken(tokenResponse.access_token,tokenResponse.refresh_token)
        userLocalDataSource.saveUserName(userName)
    }
}