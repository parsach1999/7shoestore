package com.parsa.nikestore.data.repo.order

import com.parsa.nikestore.data.Checkout
import com.parsa.nikestore.data.SubmitOrderResult
import com.parsa.nikestore.data.repo.source.order.OrderDataSource
import io.reactivex.Single

class OrderRepositoryImpl(val orderDataSource: OrderDataSource):OrderRepository {
    override fun submit(
        firstName: String,
        lastName: String,
        postalCode: String,
        mobile: String,
        address: String,
        paymentMethod: String
    ): Single<SubmitOrderResult> {
        return orderDataSource.submit(firstName,
            lastName,
            postalCode,
            mobile,
            address,
            paymentMethod
        )
    }

    override fun checkOut(orderId: Int): Single<Checkout> {
        return orderDataSource.checkOut(orderId)
    }
}