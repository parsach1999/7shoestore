package com.parsa.nikestore.data.repo.banner

import com.parsa.nikestore.data.Banner
import io.reactivex.Single

interface BannerRepository {
    fun getAll():Single<List<Banner>>
}