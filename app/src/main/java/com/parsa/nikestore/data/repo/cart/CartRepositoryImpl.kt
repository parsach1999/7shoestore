package com.parsa.nikestore.data.repo.cart

import com.parsa.nikestore.data.AddToCartResponse
import com.parsa.nikestore.data.CartItemCount
import com.parsa.nikestore.data.CartResponse
import com.parsa.nikestore.data.MessageResponse
import com.parsa.nikestore.data.repo.source.cart.CartDataSource
import io.reactivex.Single

class CartRepositoryImpl(val cartDataSource: CartDataSource): CartRepository {
    override fun addToCart(productId: Int): Single<AddToCartResponse> {
        return cartDataSource.addToCart(productId)
    }

    override fun get(): Single<CartResponse> {
        return cartDataSource.get()
    }

    override fun remove(cartItemId: Int): Single<MessageResponse> {
        return cartDataSource.remove(cartItemId)
    }

    override fun changeCount(cartItemId: Int, count: Int): Single<AddToCartResponse> {
        return cartDataSource.changeCount(cartItemId,count)
    }

    override fun getCartItemCount(): Single<CartItemCount> {
        return cartDataSource.getCartItemCount()
    }
}