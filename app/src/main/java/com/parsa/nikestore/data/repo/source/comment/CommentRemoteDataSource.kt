package com.parsa.nikestore.data.repo.source.comment

import com.parsa.nikestore.data.Comment
import com.parsa.nikestore.services.http.ApiService
import io.reactivex.Single

class CommentRemoteDataSource(val apiService: ApiService): CommentDataSource {
    override fun getAll(productId: Int): Single<List<Comment>> {
        return apiService.getComments(productId)
    }

    override fun insert(): Single<Comment> {
        TODO("Not yet implemented")
    }
}