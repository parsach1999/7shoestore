package com.parsa.nikestore.data.repo.source.order

import com.google.gson.JsonObject
import com.parsa.nikestore.data.Checkout
import com.parsa.nikestore.data.SubmitOrderResult
import com.parsa.nikestore.services.http.ApiService
import io.reactivex.Single

class OrderRemoteDataSource(val apiService: ApiService):OrderDataSource {
    override fun submit(
        firstName: String,
        lastName: String,
        postalCode: String,
        mobile: String,
        address: String,
        paymentMethod: String
    ): Single<SubmitOrderResult> {
        return apiService.submitOrder(JsonObject().apply {
            addProperty("first_name",firstName)
            addProperty("last_name",lastName)
            addProperty("postal_code",postalCode)
            addProperty("mobile",mobile)
            addProperty("address",address)
            addProperty("payment_method",paymentMethod)
        })
    }

    override fun checkOut(orderId: Int): Single<Checkout> {
        return apiService.checkout(orderId)
    }
}