package com.parsa.nikestore.data.repo.source.user

import com.parsa.nikestore.data.MessageResponse
import com.parsa.nikestore.data.TokenResponse
import io.reactivex.Completable
import io.reactivex.Single

interface UserDataSource {

    fun login(userName:String,password:String): Single<TokenResponse>

    fun signUp(userName: String,password: String): Single<MessageResponse>

    fun loadToken()

    fun saveToken(token:String,refresh:String)

    fun saveUserName(userName: String)

    fun getUserName():String

    fun signOut()

}