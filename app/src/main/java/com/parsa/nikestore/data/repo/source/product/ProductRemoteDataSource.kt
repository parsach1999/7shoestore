package com.parsa.nikestore.data.repo.source.product

import com.parsa.nikestore.data.Product
import com.parsa.nikestore.services.http.ApiService
import io.reactivex.Completable
import io.reactivex.Single

class ProductRemoteDataSource(val apiService: ApiService): ProductDataSource {

    override fun getAll(sort: Int): Single<List<Product>> {
        return apiService.getProducts(sort.toString())
    }

    override fun getFavoriteProducts(): Single<List<Product>> {
        TODO("Not yet implemented")
    }

    override fun addToFavorites(product: Product): Completable {
        TODO("Not yet implemented")
    }

    override fun deleteFromFavorites(product: Product): Completable {
        TODO("Not yet implemented")
    }
}