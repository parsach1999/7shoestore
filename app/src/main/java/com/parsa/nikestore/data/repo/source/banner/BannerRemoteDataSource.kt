package com.parsa.nikestore.data.repo.source.banner

import com.parsa.nikestore.data.Banner
import com.parsa.nikestore.services.http.ApiService
import io.reactivex.Single

class BannerRemoteDataSource(val apiService: ApiService): BannerDataSoure {
    override fun getAll(): Single<List<Banner>>{
        return apiService.getBanners()
    }
}