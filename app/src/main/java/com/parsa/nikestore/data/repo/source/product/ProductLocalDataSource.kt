package com.parsa.nikestore.data.repo.source.product

import androidx.room.*
import com.parsa.nikestore.data.Product
import io.reactivex.Completable
import io.reactivex.Single


@Dao
interface ProductLocalDataSource:ProductDataSource {
    override fun getAll(sort: Int): Single<List<Product>> {
        TODO("Not yet implemented")
    }
    @Query("SELECT * FROM product")
    override fun getFavoriteProducts(): Single<List<Product>>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    override fun addToFavorites(product: Product): Completable

    @Delete
    override fun deleteFromFavorites(product: Product): Completable
}