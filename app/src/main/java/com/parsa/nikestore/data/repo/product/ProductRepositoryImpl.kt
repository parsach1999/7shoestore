package com.parsa.nikestore.data.repo.product

import com.parsa.nikestore.data.Product
import com.parsa.nikestore.data.repo.source.product.ProductDataSource
import com.parsa.nikestore.data.repo.source.product.ProductLocalDataSource
import io.reactivex.Completable
import io.reactivex.Single

class ProductRepositoryImpl(val productDataSource: ProductDataSource,
                            val productLocalDataSource: ProductLocalDataSource
                            ): ProductRepository {


    override fun getAll(sort: Int): Single<List<Product>> {
        return productDataSource.getAll(sort)
    }

    override fun getFavoriteProducts(): Single<List<Product>> {
        return productLocalDataSource.getFavoriteProducts()
    }

    override fun addToFavorites(product: Product): Completable {
        return productLocalDataSource.addToFavorites(product)
    }

    override fun deleteFromFavorites(product: Product): Completable {
        return productLocalDataSource.deleteFromFavorites(product)
    }
}