package com.parsa.nikestore.data.db

import androidx.room.Database
import androidx.room.RoomDatabase
import com.parsa.nikestore.data.Product
import com.parsa.nikestore.data.repo.source.product.ProductLocalDataSource

@Database(entities = [Product::class],version = 1,exportSchema = false)
abstract class AppDatabase :RoomDatabase() {
    abstract fun productDao():ProductLocalDataSource
}