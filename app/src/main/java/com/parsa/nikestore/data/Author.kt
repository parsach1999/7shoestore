package com.parsa.nikestore.data

data class Author(
    val email: String
)