package com.parsa.nikestore.data

data class MessageResponse(
    val message: String
)