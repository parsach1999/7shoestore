package com.parsa.nikestore.data.repo.source.banner

import com.parsa.nikestore.data.Banner
import io.reactivex.Single

interface BannerDataSoure {
    fun getAll():Single<List<Banner>>
}