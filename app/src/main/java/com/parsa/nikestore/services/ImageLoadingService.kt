package com.parsa.nikestore.services

import com.parsa.nikestore.view.NikeImageView

interface ImageLoadingService {
    fun load(imageView: NikeImageView,url:String)
}