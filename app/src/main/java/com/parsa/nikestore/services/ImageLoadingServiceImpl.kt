package com.parsa.nikestore.services

import com.facebook.drawee.view.SimpleDraweeView
import com.parsa.nikestore.view.NikeImageView
import java.lang.IllegalStateException

class ImageLoadingServiceImpl:ImageLoadingService {
    override fun load(imageView: NikeImageView, url: String) {
        if (imageView is SimpleDraweeView){
            imageView.setImageURI(url)
        }else
            throw IllegalStateException("image view should be instance of simpledraweeview")
    }
}