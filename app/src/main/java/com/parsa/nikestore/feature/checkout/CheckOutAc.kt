package com.parsa.nikestore.feature.checkout

import android.net.Uri
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.TextView
import com.parsa.nikestore.R
import com.parsa.nikestore.common.EXTRA_ID
import com.parsa.nikestore.common.NikeActivity
import org.koin.androidx.viewmodel.ext.android.viewModel
import org.koin.core.parameter.parametersOf

class CheckOutAc : NikeActivity() {
    val viewModel:CheckOutViewModel by viewModel {
        val uri : Uri? =intent.data
        if (uri!=null)
            parametersOf(uri.getQueryParameter("order_id")!!.toInt())
        else
            parametersOf(intent.extras!!.getInt(EXTRA_ID))
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_checkout)

        val orderStateTv:TextView=findViewById(R.id.checkout_orderState)
        val orderTitleTv:TextView=findViewById(R.id.checkout_title)
        val priceTv:TextView=findViewById(R.id.checkout_price)

        viewModel.checkoutLiveData.observe(this){
            priceTv.text=it.payable_price.toString()
            orderStateTv.text=it.payment_status
            orderTitleTv.text=
                if (it.purchase_success) "خرید باموقیت انجام شد" else "خرید ناموفق"
        }
    }
}