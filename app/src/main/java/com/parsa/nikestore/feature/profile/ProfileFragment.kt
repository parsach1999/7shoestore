package com.parsa.nikestore.feature.profile

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.parsa.nikestore.R
import com.parsa.nikestore.common.NikeFragment
import com.parsa.nikestore.feature.auth.AuthAc
import com.parsa.nikestore.feature.favorite.FavoriteAc
import org.koin.androidx.viewmodel.ext.android.viewModel

class ProfileFragment: NikeFragment() {
    val viewModel:ProfileViewModel by viewModel()
    lateinit var userNameTv:TextView
    lateinit var authTv:TextView
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.profile_fragment,container,false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val favListBtn:TextView=view.findViewById(R.id.profile_favoritesTv)
        userNameTv=view.findViewById(R.id.profile_usernameTv)
        authTv=view.findViewById(R.id.profile_signInOrSignOut)


        favListBtn.setOnClickListener {
            startActivity(Intent(requireContext(),FavoriteAc::class.java))
        }


    }

    override fun onResume() {
        super.onResume()
        checkAuthState()
    }

    private fun checkAuthState(){
        if(viewModel.isSignedIn){
            authTv.text=getString(R.string.signOut)
            authTv.setCompoundDrawablesWithIntrinsicBounds(0,0,R.drawable.ic_sign_out,0)
            userNameTv.text=viewModel.userName
            authTv.setOnClickListener {
                viewModel.singOut()
                checkAuthState()
            }

        }else{
            authTv.text=getString(R.string.signIn)
            userNameTv.text=getString(R.string.guest_user)
            authTv.setCompoundDrawablesWithIntrinsicBounds(0,0,R.drawable.ic_sign_in,0)
            authTv.setOnClickListener {
                startActivity(Intent(requireContext(),AuthAc::class.java))
            }
        }
    }
}