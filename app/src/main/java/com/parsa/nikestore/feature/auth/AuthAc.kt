package com.parsa.nikestore.feature.auth

import android.os.Bundle
import android.view.View
import android.widget.FrameLayout
import com.parsa.nikestore.R
import com.parsa.nikestore.common.NikeActivity

class AuthAc : NikeActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_auth)

        supportFragmentManager.beginTransaction().apply {
            replace(R.id.auth_fragmentContainer,LoginFragment())
        }.commit()

    }
}