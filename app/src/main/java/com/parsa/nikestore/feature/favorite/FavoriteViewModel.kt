package com.parsa.nikestore.feature.favorite

import androidx.lifecycle.MutableLiveData
import com.parsa.nikestore.common.NikeCompletableObserver
import com.parsa.nikestore.common.NikeSingleObserver
import com.parsa.nikestore.common.NikeViewModel
import com.parsa.nikestore.data.Product
import com.parsa.nikestore.data.repo.product.ProductRepository
import io.reactivex.schedulers.Schedulers

class FavoriteViewModel(val productRepository: ProductRepository):NikeViewModel() {

    val productLiveData=MutableLiveData<List<Product>>()

    init {
        productRepository.getFavoriteProducts().subscribeOn(Schedulers.io())
            .subscribe(object :NikeSingleObserver<List<Product>>(compositeDisposable){
                override fun onSuccess(t: List<Product>) {
                    productLiveData.postValue(t)
                }

            })
    }

    fun remove(product: Product){
        productRepository.deleteFromFavorites(product).subscribeOn(Schedulers.io())
            .subscribe(object :NikeCompletableObserver(compositeDisposable){
                override fun onComplete() {
                    TODO("Not yet implemented")
                }
            })
    }
}