package com.parsa.nikestore.feature.auth

import com.parsa.nikestore.common.NikeViewModel
import com.parsa.nikestore.data.repo.user.UserRepository
import io.reactivex.Completable

class AuthViewModel(private val userRepository: UserRepository):NikeViewModel() {

    fun login(userName:String,password:String):Completable{
        return userRepository.login(userName,password)

    }

    fun signUp(userName: String,password: String):Completable{
        return userRepository.signUp(userName,password)
    }
}