package com.parsa.nikestore.feature.cart

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.TextureView
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.TextView
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.floatingactionbutton.ExtendedFloatingActionButton
import com.parsa.nikestore.R
import com.parsa.nikestore.common.EXTRA_DATA
import com.parsa.nikestore.common.NikeCompletableObserver
import com.parsa.nikestore.common.NikeFragment
import com.parsa.nikestore.data.CartItem
import com.parsa.nikestore.feature.auth.AuthAc
import com.parsa.nikestore.feature.product.ProductDetailAc
import com.parsa.nikestore.feature.shipping.ShippingAc
import com.parsa.nikestore.services.ImageLoadingService
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import org.koin.android.ext.android.inject
import org.koin.androidx.viewmodel.ext.android.viewModel

class CartFragment: NikeFragment(),CartItemAdapter.CartItemsCallBacks {
    val viewModel:CartViewModel by viewModel()
    var cartItemAdapter:CartItemAdapter?=null
    val imageLoadingService:ImageLoadingService by inject()
    val compositeDisposable=CompositeDisposable()
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.cart_fragment,container,false)
    }

    override fun onStart() {
        super.onStart()
        viewModel.refresh()
    }
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val payBtn=view.findViewById<ExtendedFloatingActionButton>(R.id.cart_payBtn)
        val cartItemRv=view.findViewById<RecyclerView>(R.id.cartRv)
        cartItemRv.layoutManager=
            LinearLayoutManager(requireContext(),RecyclerView.VERTICAL,false)

        viewModel.progressLiveData.observe(viewLifecycleOwner){
            setProgressIndicator(it)
        }

        viewModel.cartItemsLiveData.observe(viewLifecycleOwner){
            cartItemAdapter= CartItemAdapter(it as MutableList<CartItem>,imageLoadingService,this)
            cartItemRv.adapter=cartItemAdapter
        }
        viewModel.purchaseDetailLiveData.observe(viewLifecycleOwner){
            cartItemAdapter?.let {adapter->
                adapter.purchaseDetail=it
                adapter.notifyItemChanged(adapter.cartItems.size)
            }
        }

        viewModel.emptyStateLiveData.observe(viewLifecycleOwner){
            val emptyState=showEmptyState(R.layout.view_empty_state)
            val emptyStateRoot=view.findViewById<View>(R.id.view_emptyState)

            if (it.mustShow){
                emptyState?.let {view->
                    val emptyStateMessage=view.findViewById<TextView>(R.id.emptyState_message)
                    val emptyStateBtn=view.findViewById<Button>(R.id.emptyState_loginBtn)
                    emptyStateMessage.text=getString(it.messageResId)
                    emptyStateBtn.visibility=if (it.mustShowCallToActionBtn)View.VISIBLE else View.GONE
                    emptyStateBtn.setOnClickListener {
                        startActivity(Intent(requireContext(),AuthAc::class.java))
                    }

                }
            }else
                emptyStateRoot?.visibility=View.GONE

        }

        payBtn.setOnClickListener {
            startActivity(Intent(requireContext(),ShippingAc::class.java).apply {
                putExtra(EXTRA_DATA,viewModel.purchaseDetailLiveData.value)
            })
        }

    }

    override fun onRemoveItemClicked(cartItem: CartItem) {
        viewModel.removeItemFromCart(cartItem)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(object :NikeCompletableObserver(compositeDisposable){
                override fun onComplete() {
                    cartItemAdapter?.remove(cartItem)
                }

            })
    }

    override fun onIncreaseCartItemClicked(cartItem: CartItem) {
        viewModel.increaseCartItemCartItemCount(cartItem).subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(object :NikeCompletableObserver(compositeDisposable){
                override fun onComplete() {
                    cartItemAdapter?.changeCount(cartItem)
                }

            })
    }

    override fun onDecreaseCartItemClicked(cartItem: CartItem) {
        viewModel.decreaseCartItemCartItemCount(cartItem).subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(object :NikeCompletableObserver(compositeDisposable){
                override fun onComplete() {
                    cartItemAdapter?.changeCount(cartItem)
                }

            })
    }

    override fun onProductImageClicked(cartItem: CartItem) {
        startActivity(Intent(requireActivity(),ProductDetailAc::class.java).apply {
            putExtra(EXTRA_DATA,cartItem.product)
        })
    }

    override fun onStop() {
        super.onStop()
        compositeDisposable.clear()
    }
}