package com.parsa.nikestore.feature.favorite

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.RecoverySystem
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.parsa.nikestore.R
import com.parsa.nikestore.common.EXTRA_DATA
import com.parsa.nikestore.common.NikeActivity
import com.parsa.nikestore.data.Product
import com.parsa.nikestore.feature.product.ProductDetailAc
import org.koin.android.ext.android.get
import org.koin.androidx.viewmodel.ext.android.viewModel

class FavoriteAc : NikeActivity(),FavoriteAdapter.FavoriteProductEventListener {

    val viewModel:FavoriteViewModel by viewModel()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_favortie)

        val rv :RecyclerView =findViewById(R.id.favorites_rv)

        viewModel.productLiveData.observe(this){
            if (it.isNullOrEmpty()){
                rv.layoutManager=
                    LinearLayoutManager(this,RecyclerView.VERTICAL,false)

                rv.adapter=FavoriteAdapter(it as MutableList<Product>,this,get())
            }else
                showEmptyState(R.layout.view_default_empty_state)

        }
    }

    override fun onClick(product: Product) {
        startActivity(Intent(this,ProductDetailAc::class.java).apply {
            putExtra(EXTRA_DATA,product)
        })
    }

    override fun onLongClick(product: Product) {
        viewModel.remove(product)
    }
}