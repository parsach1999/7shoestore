package com.parsa.nikestore.feature.product.comment

import androidx.lifecycle.MutableLiveData
import com.parsa.nikestore.common.NikeSingleObserver
import com.parsa.nikestore.common.NikeViewModel
import com.parsa.nikestore.common.asyncNetworkRequest
import com.parsa.nikestore.data.Comment
import com.parsa.nikestore.data.repo.comment.CommentRepository

class CommentListViewModel(productId:Int,commentRepository: CommentRepository):NikeViewModel() {
    val commentLiveData=MutableLiveData<List<Comment>>()

    init {
        progressLiveData.value=true
        commentRepository.getAll(productId)
            .asyncNetworkRequest()
            .doFinally { progressLiveData.value=false }
            .subscribe(object :NikeSingleObserver<List<Comment>>(compositeDisposable){
                override fun onSuccess(t: List<Comment>) {
                    commentLiveData.value=t
                }

            })
    }

}