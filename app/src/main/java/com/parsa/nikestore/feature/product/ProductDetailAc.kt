package com.parsa.nikestore.feature.product

import android.content.Intent
import android.graphics.Paint
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.floatingactionbutton.ExtendedFloatingActionButton
import com.parsa.nikestore.R
import com.parsa.nikestore.common.EXTRA_ID
import com.parsa.nikestore.common.NikeActivity
import com.parsa.nikestore.common.NikeCompletableObserver
import com.parsa.nikestore.data.Comment
import com.parsa.nikestore.feature.product.comment.CommentListAc
import com.parsa.nikestore.feature.product.comment.CommentListAdapter
import com.parsa.nikestore.services.ImageLoadingService
import com.parsa.nikestore.view.NikeImageView
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import org.koin.android.ext.android.inject
import org.koin.androidx.viewmodel.ext.android.viewModel
import org.koin.core.parameter.parametersOf
import java.util.ArrayList

class ProductDetailAc : NikeActivity() {
    private val TAG = "ProductDetailAc"
    val productDetailViewModel:ProductDetailViewModel by viewModel{ parametersOf(intent.extras)}
    val imageLoadingService:ImageLoadingService by inject()
    val commentListAdapter=CommentListAdapter()
    val compositeDisposable=CompositeDisposable()
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_product_detail)

        val productIv=findViewById<NikeImageView>(R.id.productDetailIv)
        val addToCartBtn=findViewById<ExtendedFloatingActionButton>(R.id.productDetailAddToCartBtn)
        val title=findViewById<TextView>(R.id.productDetailTitleTv)
        val previousPrice=findViewById<TextView>(R.id.productDetailPreviousPriceTv)
        val currentPrice=findViewById<TextView>(R.id.productDetailCurrentPriceTv)
        val commentRv= findViewById<RecyclerView>(R.id.productDetailCommentsRv)
        val viewAllCommentsBtn= findViewById<Button>(R.id.productDetailViewAllCommentsBtn)
        val favBtn= findViewById<ImageView>(R.id.productDetailFavBtn)


        commentRv.layoutManager=
            LinearLayoutManager(this,RecyclerView.VERTICAL,false)
        commentRv.adapter=commentListAdapter

        productDetailViewModel.productLiveData.observe(this){
            imageLoadingService.load(productIv,it.image)
            title.text=it.title
            previousPrice.text=it.previous_price.toString()
            previousPrice.paintFlags=Paint.STRIKE_THRU_TEXT_FLAG
            currentPrice.text=it.price.toString()
            if (it.isFavorite)
                favBtn.setImageResource(R.drawable.ic_favorite_fill)
            else
                favBtn.setImageResource(R.drawable.ic_favorites)

            favBtn.setOnClickListener { view->
                productDetailViewModel.addToFavorites(it)
            }

        }


        productDetailViewModel.progressLiveData.observe(this){
            setProgressIndicator(it)
        }

        productDetailViewModel.commentsLiveData.observe(this){
            commentListAdapter.comments=it as ArrayList<Comment>
            if (it.size>3){
                viewAllCommentsBtn.visibility= View.VISIBLE
                viewAllCommentsBtn.setOnClickListener {
                    startActivity(Intent(this,CommentListAc::class.java).apply {
                        putExtra(EXTRA_ID,productDetailViewModel.productLiveData.value!!.id)
                    })
                }
            }
        }

        addToCartBtn.setOnClickListener {
            productDetailViewModel.addToCartBtn()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(object :NikeCompletableObserver(compositeDisposable){
                    override fun onComplete() {
                        showSnackBar(getString(R.string.successAddToCart))
                    }

                })
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        compositeDisposable.clear()
    }
}