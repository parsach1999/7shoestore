package com.parsa.nikestore.feature.product.comment

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.parsa.nikestore.R
import com.parsa.nikestore.data.Comment
import java.util.ArrayList

class CommentListAdapter(val showAll:Boolean=false): RecyclerView.Adapter<CommentListAdapter.ViewHolder>() {

    var comments=ArrayList<Comment>()
    set(value) {
        field=value
        notifyDataSetChanged()
    }

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val title=itemView.findViewById<TextView>(R.id.commentTitle)
        val date=itemView.findViewById<TextView>(R.id.commentDate)
        val author=itemView.findViewById<TextView>(R.id.commentAuthor)
        val commentContent=itemView.findViewById<TextView>(R.id.commentContent)

        fun bind(comment: Comment){
            title.text=comment.title
            date.text=comment.date
            author.text=comment.author.email
            commentContent.text=comment.content
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.item_comment,parent,false))
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(comments[position])
    }

    override fun getItemCount(): Int {
        return if (comments.size>3 && !showAll) 3 else comments.size
    }
}