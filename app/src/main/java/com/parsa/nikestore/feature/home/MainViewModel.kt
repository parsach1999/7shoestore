package com.parsa.nikestore.feature.home

import com.parsa.nikestore.common.NikeSingleObserver
import com.parsa.nikestore.common.NikeViewModel
import com.parsa.nikestore.data.CartItemCount
import com.parsa.nikestore.data.TokenContainer
import com.parsa.nikestore.data.repo.cart.CartRepository
import io.reactivex.schedulers.Schedulers
import org.greenrobot.eventbus.EventBus

class MainViewModel(private val cartRepository: CartRepository):NikeViewModel() {

    fun getCartItemCount(){
        if (!TokenContainer.token.isNullOrEmpty()){
            cartRepository.getCartItemCount()
                .subscribeOn(Schedulers.io())
                .subscribe(object :NikeSingleObserver<CartItemCount>(compositeDisposable){
                    override fun onSuccess(t: CartItemCount) {
                        EventBus.getDefault().postSticky(t)
                    }

                })
        }
    }
}