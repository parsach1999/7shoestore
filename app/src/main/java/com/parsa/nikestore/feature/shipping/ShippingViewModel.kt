package com.parsa.nikestore.feature.shipping

import com.parsa.nikestore.common.NikeViewModel
import com.parsa.nikestore.data.Checkout
import com.parsa.nikestore.data.SubmitOrderResult
import com.parsa.nikestore.data.repo.order.OrderRepository
import io.reactivex.Single
const val PAYMENT_METHOD_COD="cash_on_delivery"
const val PAYMENT_METHOD_ONLINE="online"
class ShippingViewModel(private val orderRepository: OrderRepository): NikeViewModel() {

    fun submit(
        firstName:String,
        lastName:String,
        postalCode:String,
        mobile:String,
        address:String,
        paymentMethod:String
    ):Single<SubmitOrderResult>{
        return orderRepository.submit(firstName,
        lastName,
        postalCode,
        mobile,
        address,
        paymentMethod)
    }

    fun checkOut(orderId:Int):Single<Checkout>{
        return orderRepository.checkOut(orderId)
    }
}