package com.parsa.nikestore.feature.shipping

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.View
import android.widget.TextView
import com.google.android.material.button.MaterialButton
import com.google.android.material.textfield.TextInputEditText
import com.google.android.material.textfield.TextInputLayout
import com.parsa.nikestore.R
import com.parsa.nikestore.common.*
import com.parsa.nikestore.data.PurchaseDetail
import com.parsa.nikestore.data.SubmitOrderResult
import com.parsa.nikestore.feature.checkout.CheckOutAc
import io.reactivex.disposables.CompositeDisposable
import org.koin.androidx.viewmodel.ext.android.viewModel
import retrofit2.http.Url
import java.lang.IllegalStateException

class ShippingAc : NikeActivity() {
    val compositeDisposable=CompositeDisposable()
    val viewModel:ShippingViewModel by viewModel()
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_shipping)

        val firstNameEt:TextInputEditText=findViewById(R.id.shipping_firstNameEt)
        val lastNameEt:TextInputEditText=findViewById(R.id.shipping_lastNameEt)
        val postalCodeEt:TextInputEditText=findViewById(R.id.shipping_postalCodeEt)
        val addressEt:TextInputEditText=findViewById(R.id.shipping_addressEt)
        val mobileEt:TextInputEditText=findViewById(R.id.shipping_mobileEt)

        val firstNameEtl:TextInputLayout=findViewById(R.id.shipping_firstNameEtl)
        val lastNameEtl:TextInputLayout=findViewById(R.id.shipping_lastNameEtl)
        val postalCodeEtl:TextInputLayout=findViewById(R.id.shipping_postalCodeEtl)
        val addressEtl:TextInputLayout=findViewById(R.id.shipping_addressEtl)
        val mobileEtl:TextInputLayout=findViewById(R.id.shipping_mobileEtl)

        val onlinePaymentBtn:MaterialButton=findViewById(R.id.shipping_onlinePaymentBtn)
        val codPaymentBtn:MaterialButton=findViewById(R.id.shipping_codPaymentBtn)

        val payablePrice:TextView=findViewById(R.id.shipping_payablePriceTv)
        val shippingCost:TextView=findViewById(R.id.shipping_shippingCostTv)
        val totalPrice:TextView=findViewById(R.id.shipping_totalPriceTv)

        val purchaseDetail=intent.getParcelableExtra<PurchaseDetail>(EXTRA_DATA)?:
        throw IllegalStateException("purchase detail cannot be null")

        payablePrice.text=purchaseDetail.payable_price.toString()
        shippingCost.text=purchaseDetail.shipping_cost.toString()
        totalPrice.text=purchaseDetail.total_price.toString()

        val onClick = View.OnClickListener {
            if (firstNameEt.length()>0 &&
                    lastNameEt.length()>0 &&
                    postalCodeEt.length()>9 &&
                    mobileEt.length()>10 &&
                    addressEt.length()>20){

                viewModel.submit(firstNameEt.text.toString(),
                lastNameEt.text.toString(),postalCodeEt.text.toString(),mobileEt.text.toString(),
                addressEt.text.toString(),if (it.id==R.id.shipping_onlinePaymentBtn) PAYMENT_METHOD_ONLINE else PAYMENT_METHOD_COD)
                    .asyncNetworkRequest()
                    .subscribe(object :NikeSingleObserver<SubmitOrderResult>(compositeDisposable){
                        override fun onSuccess(t: SubmitOrderResult) {
                            if (t.bank_gateway_url.isNotEmpty()){
                                openUrlInCustomTab(this@ShippingAc,t.bank_gateway_url)
                            }else{
                                startActivity(Intent(this@ShippingAc,CheckOutAc::class.java).apply {
                                    putExtra(EXTRA_ID,t.order_id)
                                })
                            }
                            finish()

                        }

                    })

            }else {
                if (firstNameEt.length()==0)
                    firstNameEtl.error="نام نباید خالی باشد"
                if (lastNameEt.length()==0)
                    lastNameEtl.error="نام خانوادگی نباید خالی باشد"
                if (addressEt.length()<20)
                    addressEtl.error="آدرس باید حداقل بیست حرف باشد"
                if (mobileEt.length()<10)
                    mobileEtl.error="شماره همراه صحیح نیست"
                if (postalCodeEt.length()<11)
                    postalCodeEtl.error="کد پستی صحیح نیست"
            }


        }

        onlinePaymentBtn.setOnClickListener(onClick)
        codPaymentBtn.setOnClickListener(onClick)


    }

    override fun onDestroy() {
        super.onDestroy()
        compositeDisposable.clear()
    }
}