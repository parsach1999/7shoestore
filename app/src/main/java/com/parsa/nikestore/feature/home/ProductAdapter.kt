package com.parsa.nikestore.feature.home

import android.graphics.Paint
import android.view.LayoutInflater
import android.view.TextureView
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.parsa.nikestore.R
import com.parsa.nikestore.common.implementSpringAnimationTrait
import com.parsa.nikestore.data.Product
import com.parsa.nikestore.services.ImageLoadingService
import com.parsa.nikestore.view.NikeImageView
import java.lang.IllegalStateException
import java.util.ArrayList

const val VIEW_TYPE_ROUND=0
const val VIEW_TYPE_SMALL=1
const val VIEW_TYPE_LARGE=2

class ProductAdapter(var viewType: Int,val imageLoadingService: ImageLoadingService): RecyclerView.Adapter<ProductAdapter.ViewHolder>() {

    var productEventListener:ProductEventListener?=null

    var products=ArrayList<Product>()
        set(value) {
            field=value
            notifyDataSetChanged()
        }
    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val productIv=itemView.findViewById<NikeImageView>(R.id.productIv)
        val productTitleTv=itemView.findViewById<TextView>(R.id.productTitleTv)
        val previousPriceTv=itemView.findViewById<TextView>(R.id.previousPriceTv)
        val currentPriceTv=itemView.findViewById<TextView>(R.id.currentPriceTv)
        val favBtn=itemView.findViewById<ImageView>(R.id.favBtn)

        fun bind(product: Product){
            imageLoadingService.load(productIv,product.image)
            productTitleTv.text=product.title
            previousPriceTv.text=product.previous_price.toString()
            previousPriceTv.paintFlags= Paint.STRIKE_THRU_TEXT_FLAG
            currentPriceTv.text=product.price.toString()
            itemView.implementSpringAnimationTrait()
            itemView.setOnClickListener {
                productEventListener?.onProductClicked(product)
            }

            if (product.isFavorite){
                favBtn.setImageResource(R.drawable.ic_favorite_fill)
            }
            else
                favBtn.setImageResource(R.drawable.ic_favorites)
            favBtn.setOnClickListener{
                productEventListener?.onFavBtnClicked(product)

                product.isFavorite=!product.isFavorite
                notifyItemChanged(adapterPosition)
            }


        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val layOutResId= when (viewType){
            VIEW_TYPE_ROUND->R.layout.item_product
            VIEW_TYPE_SMALL->R.layout.item_product_small
            VIEW_TYPE_LARGE->R.layout.item_product_large
            else -> throw IllegalStateException("ViewType is not Valid")
        }
        return ViewHolder(LayoutInflater.from(parent.context).inflate(layOutResId,parent,false))
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(products[position])
    }

    override fun getItemViewType(position: Int): Int {
        return viewType
    }
    override fun getItemCount(): Int {
        return products.size
    }

    interface ProductEventListener{
        fun onProductClicked(product: Product)
        fun onFavBtnClicked(product: Product)

    }

}