package com.parsa.nikestore.feature.list

import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import com.parsa.nikestore.R
import com.parsa.nikestore.common.EXTRA_DATA
import com.parsa.nikestore.common.NikeActivity
import com.parsa.nikestore.data.Product
import com.parsa.nikestore.feature.home.ProductAdapter
import com.parsa.nikestore.feature.home.VIEW_TYPE_LARGE
import com.parsa.nikestore.feature.home.VIEW_TYPE_SMALL
import com.parsa.nikestore.feature.product.ProductDetailAc
import org.koin.android.ext.android.inject
import org.koin.androidx.viewmodel.ext.android.viewModel
import org.koin.core.parameter.parametersOf
import java.util.ArrayList

class ProductListAc : NikeActivity(),ProductAdapter.ProductEventListener {
    val adapter:ProductAdapter by inject { parametersOf(VIEW_TYPE_SMALL) }
    val viewModel:ProductListViewModel by viewModel {
        parametersOf(intent.extras!!.getInt(EXTRA_DATA)) }
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_product_list)

        val sortBtn=findViewById<View>(R.id.productListSortBtn)
        val sortTv=findViewById<TextView>(R.id.productListSortTv)
        val gridLayoutManager= GridLayoutManager(this,2)
        val productRv=findViewById<RecyclerView>(R.id.rvProductLisAc)
        val viewTypeBtn=findViewById<ImageView>(R.id.productListViewType)

        productRv.layoutManager=gridLayoutManager
        productRv.adapter=adapter
        adapter.productEventListener=this
        viewTypeBtn.setOnClickListener {
            if (adapter.viewType== VIEW_TYPE_SMALL){
                viewTypeBtn.setImageResource(R.drawable.ic_plus_square)
                adapter.viewType= VIEW_TYPE_LARGE
                gridLayoutManager.spanCount=1
                adapter.notifyDataSetChanged()
            } else{
                viewTypeBtn.setImageResource(R.drawable.ic_grid)
                adapter.viewType= VIEW_TYPE_SMALL
                gridLayoutManager.spanCount=2
                adapter.notifyDataSetChanged()
            }
        }

        viewModel.progressLiveData.observe(this){
            setProgressIndicator(it)
        }

        viewModel.productsLiveData.observe(this){
            adapter.products=it as ArrayList<Product>
        }

        viewModel.selectedSortLiveData.observe(this){
            sortTv.text=getString(it)
        }

        sortBtn.setOnClickListener {
            val dialog=MaterialAlertDialogBuilder(this)
                .setSingleChoiceItems(R.array.sortArray,viewModel.sort){dialog,selectSortIndex->
                    viewModel.onSelectedSortChangedByUser(selectSortIndex)
                    dialog.dismiss()
                }.setTitle(getString(R.string.sort))
            dialog.show()
        }
    }

    override fun onProductClicked(product: Product) {
        startActivity(Intent(this, ProductDetailAc::class.java).apply {
            putExtra(EXTRA_DATA,product)
        })
    }

    override fun onFavBtnClicked(product: Product) {
        viewModel.addToFavorites(product)
    }
}