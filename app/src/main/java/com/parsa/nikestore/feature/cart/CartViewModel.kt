package com.parsa.nikestore.feature.cart

import androidx.lifecycle.MutableLiveData
import com.parsa.nikestore.R
import com.parsa.nikestore.common.NikeSingleObserver
import com.parsa.nikestore.common.NikeViewModel
import com.parsa.nikestore.common.asyncNetworkRequest
import com.parsa.nikestore.data.*
import com.parsa.nikestore.data.repo.cart.CartRepository
import io.reactivex.Completable
import org.greenrobot.eventbus.EventBus

class CartViewModel(val cartRepository: CartRepository):NikeViewModel() {

    val cartItemsLiveData=MutableLiveData<List<CartItem>>()
    val purchaseDetailLiveData=MutableLiveData<PurchaseDetail>()
    val emptyStateLiveData=MutableLiveData<EmptyState>()

    private fun getCartItemCart(){
        if (!TokenContainer.token.isNullOrEmpty()){
            progressLiveData.value=true
            emptyStateLiveData.value= EmptyState(false)
            cartRepository.get()
                .asyncNetworkRequest()
                .doFinally { progressLiveData.value=false }
                .subscribe(object :NikeSingleObserver<CartResponse>(compositeDisposable){
                    override fun onSuccess(t: CartResponse) {
                        if (t.cart_items.isNotEmpty()) {
                            cartItemsLiveData.value = t.cart_items
                            purchaseDetailLiveData.value=PurchaseDetail(t.total_price,t.shipping_cost,t.payable_price)

                        }else
                            emptyStateLiveData.value= EmptyState(true, R.string.cartEmptyState)
                    }

                })
        }else
            emptyStateLiveData.value= EmptyState(true,R.string.cartEmptyStateLoginRequired,true)

    }

    fun removeItemFromCart(cartItem:CartItem):Completable{
        return cartRepository.remove(cartItem.cart_item_id)
            .doAfterSuccess { calculateAndPublishPurchaseDetail()
                val cartItemCount=EventBus.getDefault().getStickyEvent(CartItemCount::class.java)
                cartItemCount?.let {
                    it.count-=cartItem.count
                    EventBus.getDefault().postSticky(it)
                }
                cartItemsLiveData.value?.let {
                    if (it.isEmpty()){

                        emptyStateLiveData.postValue(EmptyState(true,R.string.cartEmptyState))

                    }
                }
            }
            .ignoreElement()
    }

    fun increaseCartItemCartItemCount(cartItem: CartItem):Completable{
        return cartRepository.changeCount(cartItem.cart_item_id,++cartItem.count)
            .doAfterSuccess {
                calculateAndPublishPurchaseDetail()
                val cartItemCount=EventBus.getDefault().getStickyEvent(CartItemCount::class.java)
                cartItemCount?.let {
                    it.count+=1
                    EventBus.getDefault().postSticky(it)
                }

            }
            .ignoreElement()
    }

    fun decreaseCartItemCartItemCount(cartItem: CartItem):Completable{
        return cartRepository
            .changeCount(cartItem.cart_item_id,--cartItem.count)
            .doAfterSuccess {
                calculateAndPublishPurchaseDetail()
                val cartItemCount=EventBus.getDefault().getStickyEvent(CartItemCount::class.java)
                cartItemCount?.let {
                    it.count-=1
                    EventBus.getDefault().postSticky(it)
                }
            }
            .ignoreElement()
    }

    fun refresh(){
        getCartItemCart()
    }

    private fun calculateAndPublishPurchaseDetail(){
        cartItemsLiveData.value?.let { cartItems->
            purchaseDetailLiveData.value?.let {
                var totalPrice=0
                var payablePrice=0
                cartItems.forEach{
                    totalPrice+=it.product.price*it.count
                    payablePrice+=(it.product.price-it.product.discount)*it.count
                    }
                it.payable_price=payablePrice
                it.total_price=totalPrice
                purchaseDetailLiveData.postValue(it)

            }
        }
    }
}