package com.parsa.nikestore.feature.product

import android.os.Bundle
import androidx.lifecycle.MutableLiveData
import com.parsa.nikestore.common.*
import com.parsa.nikestore.data.Comment
import com.parsa.nikestore.data.Product
import com.parsa.nikestore.data.repo.cart.CartRepository
import com.parsa.nikestore.data.repo.comment.CommentRepository
import com.parsa.nikestore.data.repo.product.ProductRepository
import io.reactivex.Completable
import io.reactivex.schedulers.Schedulers

class ProductDetailViewModel(bundle: Bundle,val productRepository: ProductRepository ,commentRepository: CommentRepository, val cartRepository: CartRepository):NikeViewModel() {
    val productLiveData=MutableLiveData<Product>()
    val commentsLiveData=MutableLiveData<List<Comment>>()
    init {
        progressLiveData.value=true
        productLiveData.value=bundle.getParcelable(EXTRA_DATA)
        commentRepository.getAll(productLiveData.value!!.id)
            .asyncNetworkRequest()
            .doFinally { progressLiveData.value=false }
            .subscribe(object :NikeSingleObserver<List<Comment>>(compositeDisposable){
                override fun onSuccess(t: List<Comment>) {
                    commentsLiveData.value=t
                }

            })
    }
    fun addToCartBtn(): Completable {
        return cartRepository.addToCart(productLiveData.value!!.id)
            .ignoreElement()
    }

    fun addToFavorites(product:Product){
        if (product.isFavorite){
            productRepository.deleteFromFavorites(product).subscribeOn(Schedulers.io())
                .subscribe(object : NikeCompletableObserver(compositeDisposable){
                    override fun onComplete() {
                        product.isFavorite=false
                    }
                })
        }else{
            productRepository.addToFavorites(product).subscribeOn(Schedulers.io())
                .subscribe(object : NikeCompletableObserver(compositeDisposable){
                    override fun onComplete() {
                        product.isFavorite=true
                    }
                })
        }
    }
}