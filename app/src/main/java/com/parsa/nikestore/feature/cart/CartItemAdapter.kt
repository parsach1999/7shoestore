package com.parsa.nikestore.feature.cart

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.ProgressBar
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.parsa.nikestore.R
import com.parsa.nikestore.data.CartItem
import com.parsa.nikestore.data.PurchaseDetail
import com.parsa.nikestore.services.ImageLoadingService
import com.parsa.nikestore.view.NikeImageView

const val CART_ITEM=0
const val PURCHASE_DETAIL=1
class CartItemAdapter(val cartItems : MutableList<CartItem>,
    val imageLoadingService: ImageLoadingService,
    val callBack:CartItemsCallBacks): RecyclerView.Adapter<RecyclerView.ViewHolder>() {
    var purchaseDetail:PurchaseDetail?=null
    inner class CartItemViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val productTitle=itemView.findViewById<TextView>(R.id.cart_productTitleTv)
        val productIv=itemView.findViewById<NikeImageView>(R.id.cart_productIv)
        val countTv=itemView.findViewById<TextView>(R.id.cart_countTv)
        val previousPriceTv=itemView.findViewById<TextView>(R.id.cart_previousPriceTv)
        val priceTv=itemView.findViewById<TextView>(R.id.cart_currentPrice)
        val removeBtn=itemView.findViewById<TextView>(R.id.cart_remove)
        val increaseBtn=itemView.findViewById<ImageView>(R.id.cart_increaseCount)
        val decreaseBtn=itemView.findViewById<ImageView>(R.id.cart_decreaseCount)
        val progressBar=itemView.findViewById<ProgressBar>(R.id.cart_progressBar)
        fun bindCartItems(cartItem: CartItem){
            productTitle.text=cartItem.product.title
            imageLoadingService.load(productIv,cartItem.product.image)
            countTv.text=cartItem.count.toString()
            previousPriceTv.text= (cartItem.product.price + cartItem.product.discount).toString()
            priceTv.text=cartItem.product.price.toString()

            removeBtn.setOnClickListener {
                callBack.onRemoveItemClicked(cartItem)
            }

            progressBar.visibility=
                if (cartItem.progressBarVisibility) View.VISIBLE else View.GONE
            countTv.visibility=
                if (cartItem.progressBarVisibility) View.INVISIBLE else View.VISIBLE
            increaseBtn.setOnClickListener {
                cartItem.progressBarVisibility=true
                progressBar.visibility=View.VISIBLE
                countTv.visibility=View.INVISIBLE
                callBack.onIncreaseCartItemClicked(cartItem)
            }

            decreaseBtn.setOnClickListener {
                if (cartItem.count>1){
                    cartItem.progressBarVisibility=true
                    progressBar.visibility=View.VISIBLE
                    countTv.visibility=View.INVISIBLE
                    callBack.onDecreaseCartItemClicked(cartItem)
                }
            }

            productIv.setOnClickListener {
                callBack.onProductImageClicked(cartItem)
            }


        }
    }

    inner class PurchaseDetailViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val totalPriceTv=itemView.findViewById<TextView>(R.id.totalPriceTv)
        val shippingCostTv=itemView.findViewById<TextView>(R.id.shippingCostTv)
        val payablePriceTv=itemView.findViewById<TextView>(R.id.payablePriceTv)

        fun bind(totalPrice:Int,shippingCost:Int,payablePrice:Int){
            totalPriceTv.text=totalPrice.toString()
            shippingCostTv.text=shippingCost.toString()
            payablePriceTv.text=payablePrice.toString()
        }

    }

    interface CartItemsCallBacks{
        fun onRemoveItemClicked(cartItem: CartItem)
        fun onIncreaseCartItemClicked(cartItem: CartItem)
        fun onDecreaseCartItemClicked(cartItem: CartItem)
        fun onProductImageClicked(cartItem: CartItem)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        if (viewType== CART_ITEM){
            return CartItemViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.item_cart,parent,false))
        }else
            return PurchaseDetailViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.item_purchase_details,parent,false))
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        if (holder is CartItemViewHolder)
            holder.bindCartItems(cartItems[position])
        else if (holder is PurchaseDetailViewHolder)
            purchaseDetail?.let {
                holder.bind(it.total_price,it.shipping_cost,it.payable_price)
            }

    }

    override fun getItemViewType(position: Int): Int {
        if (position==cartItems.size)
            return PURCHASE_DETAIL
        else
            return CART_ITEM
    }

    override fun getItemCount(): Int {
        return cartItems.size+1
    }

    fun remove(cartItem: CartItem){
        val index=cartItems.indexOf(cartItem)
        if(index>-1){
            cartItems.removeAt(index)
            notifyItemRemoved(index)
        }
    }

    fun changeCount(cartItem: CartItem){
        val index=cartItems.indexOf(cartItem)
        if(index>-1){
            cartItems[index].progressBarVisibility=false
            notifyItemChanged(index)
        }
    }
}