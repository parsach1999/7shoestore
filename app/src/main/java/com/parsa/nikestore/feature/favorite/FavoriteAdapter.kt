package com.parsa.nikestore.feature.favorite

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.parsa.nikestore.R
import com.parsa.nikestore.data.Product
import com.parsa.nikestore.services.ImageLoadingService
import com.parsa.nikestore.view.NikeImageView

class FavoriteAdapter(val products:MutableList<Product>,
                      val favoriteProductEventListener: FavoriteProductEventListener,
                      val imageLoadingService: ImageLoadingService
                      ):
    RecyclerView.Adapter<FavoriteAdapter.ViewHolder>() {

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val productTitle:TextView=itemView.findViewById(R.id.favorites_productNameTv)
        val image:NikeImageView=itemView.findViewById(R.id.favorites_productIv)

        fun bind(product: Product){
            imageLoadingService.load(image,product.image)
            productTitle.text=product.title

            itemView.setOnClickListener{
                favoriteProductEventListener.onClick(product)
            }

            itemView.setOnLongClickListener {
                products.remove(product)
                notifyItemRemoved(adapterPosition)
                favoriteProductEventListener.onLongClick(product)
                return@setOnLongClickListener false
            }
        }


    }


    interface FavoriteProductEventListener{
        fun onClick(product: Product)
        fun onLongClick(product: Product)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.item_favorite,parent,false))
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(products[position])
    }

    override fun getItemCount(): Int {
        return products.size
    }
}