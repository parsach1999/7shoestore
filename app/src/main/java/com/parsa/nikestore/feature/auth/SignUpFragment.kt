package com.parsa.nikestore.feature.auth

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.EditText
import android.widget.Toast
import androidx.fragment.app.Fragment
import com.parsa.nikestore.R
import com.parsa.nikestore.common.NikeCompletableObserver
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import org.koin.androidx.viewmodel.ext.android.viewModel

class SignUpFragment: Fragment() {
    val viewModel:AuthViewModel by viewModel()
    val compositeDisposable=CompositeDisposable()
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_signup,container,false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val emailEt=view.findViewById<EditText>(R.id.signUpEmailEt)
        val passwordEt=view.findViewById<EditText>(R.id.signUpPasswordEt)
        val signUpBtn=view.findViewById<Button>(R.id.signUpBtn)
        val signUpToLoginBtn=view.findViewById<Button>(R.id.loginLinkBtn)
        signUpBtn.setOnClickListener {
            viewModel.signUp(emailEt.text.toString(),passwordEt.text.toString())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(object :NikeCompletableObserver(compositeDisposable){
                    override fun onComplete() {
                        Toast.makeText(context,getString(R.string.successful_signUp),Toast.LENGTH_SHORT).show()
                        requireActivity().finish()
                    }

                })

        }

        signUpToLoginBtn.setOnClickListener {
            requireActivity().supportFragmentManager.beginTransaction().apply {
                replace(R.id.auth_fragmentContainer,LoginFragment())
            }.commit()
        }
    }

    override fun onStop() {
        super.onStop()
        compositeDisposable.clear()
    }
}