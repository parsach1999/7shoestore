package com.parsa.nikestore.feature.checkout

import androidx.lifecycle.MutableLiveData
import com.parsa.nikestore.common.NikeSingleObserver
import com.parsa.nikestore.common.NikeViewModel
import com.parsa.nikestore.common.asyncNetworkRequest
import com.parsa.nikestore.data.Checkout
import com.parsa.nikestore.data.repo.order.OrderRepository

class CheckOutViewModel(orderId:Int, orderRepository: OrderRepository):NikeViewModel() {

    val checkoutLiveData=MutableLiveData<Checkout>()

    init {
        orderRepository.checkOut(orderId)
            .asyncNetworkRequest()
            .subscribe(object :NikeSingleObserver<Checkout>(compositeDisposable){
                override fun onSuccess(t: Checkout) {
                    checkoutLiveData.value=t
                }
            })
    }


}