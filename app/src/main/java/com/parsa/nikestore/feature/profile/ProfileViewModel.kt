package com.parsa.nikestore.feature.profile

import com.parsa.nikestore.common.NikeViewModel
import com.parsa.nikestore.data.TokenContainer
import com.parsa.nikestore.data.repo.user.UserRepository

class ProfileViewModel(private val userRepository: UserRepository):NikeViewModel() {

    val userName:String
    get() = userRepository.getUserName()

    val isSignedIn:Boolean
    get() = TokenContainer.token!=null

    fun singOut(){
        userRepository.signOut()
    }

}