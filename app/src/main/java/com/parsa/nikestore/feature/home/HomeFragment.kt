package com.parsa.nikestore.feature.home

import android.content.Intent
import android.icu.lang.UCharacter.DecompositionType.SMALL
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.viewpager2.widget.ViewPager2
import com.parsa.nikestore.R
import com.parsa.nikestore.common.EXTRA_DATA
import com.parsa.nikestore.common.NikeFragment
import com.parsa.nikestore.common.convertDpToPixel
import com.parsa.nikestore.data.*
import com.parsa.nikestore.feature.list.ProductListAc
import com.parsa.nikestore.feature.product.ProductDetailAc
import com.tbuonomo.viewpagerdotsindicator.WormDotsIndicator
import org.koin.android.ext.android.inject
import org.koin.androidx.viewmodel.ext.android.viewModel
import org.koin.core.parameter.parametersOf
import java.util.ArrayList

class HomeFragment: NikeFragment(),ProductAdapter.ProductEventListener {
    val homeViewModel:HomeViewModel by viewModel()
    val latestProductsAD:ProductAdapter by inject{ parametersOf(VIEW_TYPE_ROUND) }
    val popularProductsAD:ProductAdapter by inject{ parametersOf(VIEW_TYPE_ROUND) }
    val cheapProductsAD:ProductAdapter by inject{ parametersOf(VIEW_TYPE_ROUND) }
    val expensiveProductsAD:ProductAdapter by inject{ parametersOf(VIEW_TYPE_ROUND) }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.home_fragment,container,false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        latestProductsAD.productEventListener=this
        popularProductsAD.productEventListener=this
        cheapProductsAD.productEventListener=this
        expensiveProductsAD.productEventListener=this

        val viewLatestBtn=view.findViewById<Button>(R.id.viewLatestProductBtn)
        val viewPopularBtn=view.findViewById<Button>(R.id.viewPopularProductBtn)
        val viewExpensiveBtn=view.findViewById<Button>(R.id.viewExpensiveProductBtn)
        val viewCheapBtn=view.findViewById<Button>(R.id.viewCheapProductBtn)

        viewLatestBtn.setOnClickListener {
            startActivity(Intent(activity,ProductListAc::class.java).apply {
                putExtra(EXTRA_DATA, SORT_LATEST)
            })
        }

        viewPopularBtn.setOnClickListener {
            startActivity(Intent(activity,ProductListAc::class.java).apply {
                putExtra(EXTRA_DATA, SORT_POPULAR)
            })
        }

        viewExpensiveBtn.setOnClickListener {
            startActivity(Intent(activity,ProductListAc::class.java).apply {
                putExtra(EXTRA_DATA, SORT_EXPENSIVE)
            })
        }

        viewCheapBtn.setOnClickListener {
            startActivity(Intent(activity,ProductListAc::class.java).apply {
                putExtra(EXTRA_DATA, SORT_CHEAP)
            })
        }
        val latestProductRv=view.findViewById<RecyclerView>(R.id.rv_latest)
        latestProductRv.layoutManager=
            LinearLayoutManager(requireContext(),RecyclerView.HORIZONTAL,false)
        latestProductRv.adapter=latestProductsAD

        val popularProductRv=view.findViewById<RecyclerView>(R.id.rv_popular)
        popularProductRv.layoutManager=
            LinearLayoutManager(requireContext(),RecyclerView.HORIZONTAL,false)
        popularProductRv.adapter=popularProductsAD

        val cheapProductRv=view.findViewById<RecyclerView>(R.id.rv_cheap)
        cheapProductRv.layoutManager=
            LinearLayoutManager(requireContext(),RecyclerView.HORIZONTAL,false)
        cheapProductRv.adapter=cheapProductsAD

        val expensiveProductRv=view.findViewById<RecyclerView>(R.id.rv_expensive)
        expensiveProductRv.layoutManager=
            LinearLayoutManager(requireContext(),RecyclerView.HORIZONTAL,false)
        expensiveProductRv.adapter=expensiveProductsAD

        homeViewModel.progressLiveData.observe(viewLifecycleOwner){
            setProgressIndicator(it)
        }

        homeViewModel.bannersLiveData.observe(viewLifecycleOwner){
            val bannerStateAdapter=BannerStateAdapter(this,it)
            val bannerSlider=view.findViewById<ViewPager2>(R.id.bannerSliderViewPager)
            bannerSlider.adapter=bannerStateAdapter
            val height=(((bannerSlider.measuredWidth-convertDpToPixel(32f,requireContext()))*173)/328).toInt()
            val layParams=bannerSlider.layoutParams
            layParams.height=height

            val indecator=view.findViewById<WormDotsIndicator>(R.id.worm_dots_indicator)
            indecator.setViewPager2(bannerSlider)
        }

        homeViewModel.latestProduct.observe(viewLifecycleOwner){
            latestProductsAD.products=it as ArrayList<Product>
        }

        homeViewModel.popularProduct.observe(viewLifecycleOwner){
            popularProductsAD.products=it as ArrayList<Product>
        }

        homeViewModel.cheapProduct.observe(viewLifecycleOwner){
            cheapProductsAD.products=it as ArrayList<Product>
        }

        homeViewModel.expensiveProduct.observe(viewLifecycleOwner){
            expensiveProductsAD.products=it as ArrayList<Product>
        }


    }

    override fun onProductClicked(product: Product) {
        startActivity(Intent(activity,ProductDetailAc::class.java).apply {
            putExtra(EXTRA_DATA,product)
        })
    }

    override fun onFavBtnClicked(product: Product) {
        homeViewModel.addToFavorites(product)
    }
}