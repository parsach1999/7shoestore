package com.parsa.nikestore.feature.home

import androidx.lifecycle.MutableLiveData
import com.parsa.nikestore.common.NikeCompletableObserver
import com.parsa.nikestore.common.NikeSingleObserver
import com.parsa.nikestore.common.NikeViewModel
import com.parsa.nikestore.common.asyncNetworkRequest
import com.parsa.nikestore.data.*
import com.parsa.nikestore.data.repo.banner.BannerRepository
import com.parsa.nikestore.data.repo.product.ProductRepository
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

class HomeViewModel(val productRepository: ProductRepository,
                    bannerRepository: BannerRepository
): NikeViewModel() {

    val latestProduct=MutableLiveData<List<Product>>()
    val popularProduct=MutableLiveData<List<Product>>()
    val cheapProduct=MutableLiveData<List<Product>>()
    val expensiveProduct=MutableLiveData<List<Product>>()
    val bannersLiveData=MutableLiveData<List<Banner>>()
    init {
        progressLiveData.value=true
        productRepository.getAll(SORT_LATEST)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .doFinally { progressLiveData.value=false }
            .subscribe(object :NikeSingleObserver<List<Product>>(compositeDisposable){
                override fun onSuccess(t: List<Product>) {
                    latestProduct.value=t
                }

            })

        productRepository.getAll(SORT_POPULAR)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .doFinally { progressLiveData.value=false }
            .subscribe(object :NikeSingleObserver<List<Product>>(compositeDisposable){
                override fun onSuccess(t: List<Product>) {
                    popularProduct.value=t
                }

            })

        productRepository.getAll(SORT_CHEAP)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .doFinally { progressLiveData.value=false }
            .subscribe(object :NikeSingleObserver<List<Product>>(compositeDisposable) {
                override fun onSuccess(t: List<Product>) {
                    cheapProduct.value = t
                }
            })

        productRepository.getAll(SORT_EXPENSIVE)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .doFinally { progressLiveData.value=false }
            .subscribe(object :NikeSingleObserver<List<Product>>(compositeDisposable) {
                override fun onSuccess(t: List<Product>) {
                    expensiveProduct.value = t
                }
            })

        bannerRepository.getAll()
            .asyncNetworkRequest()
            .doFinally { progressLiveData.value=false }
            .subscribe(object :NikeSingleObserver<List<Banner>>(compositeDisposable){
                override fun onSuccess(t: List<Banner>) {
                    bannersLiveData.value=t
                }

            })
    }

    fun addToFavorites(product:Product){
        if (product.isFavorite){
            productRepository.deleteFromFavorites(product).subscribeOn(Schedulers.io())
                .subscribe(object :NikeCompletableObserver(compositeDisposable){
                    override fun onComplete() {
                        product.isFavorite=false
                    }
                })
        }else{
            productRepository.addToFavorites(product).subscribeOn(Schedulers.io())
                .subscribe(object :NikeCompletableObserver(compositeDisposable){
                    override fun onComplete() {
                        product.isFavorite=true
                    }
                })
        }
    }

}