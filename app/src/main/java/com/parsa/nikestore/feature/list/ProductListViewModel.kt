package com.parsa.nikestore.feature.list

import androidx.lifecycle.MutableLiveData
import com.parsa.nikestore.R
import com.parsa.nikestore.common.NikeCompletableObserver
import com.parsa.nikestore.common.NikeSingleObserver
import com.parsa.nikestore.common.NikeViewModel
import com.parsa.nikestore.common.asyncNetworkRequest
import com.parsa.nikestore.data.Product
import com.parsa.nikestore.data.repo.product.ProductRepository
import io.reactivex.schedulers.Schedulers

class ProductListViewModel(var sort:Int , val productRepository: ProductRepository):NikeViewModel() {
    val productsLiveData=MutableLiveData<List<Product>>()
    val selectedSortLiveData=MutableLiveData<Int>()
    val sortTitle= arrayOf(
        R.string.latest,
        R.string.popular,
        R.string.mostCheap,
        R.string.mostExpensive
    )

    init {
        getProducts()
        selectedSortLiveData.value=sortTitle[sort]
    }

    fun getProducts(){
        progressLiveData.value=true
        productRepository.getAll(sort)
            .asyncNetworkRequest()
            .doFinally { progressLiveData.value=false }
            .subscribe(object :NikeSingleObserver<List<Product>>(compositeDisposable){
                override fun onSuccess(t: List<Product>) {
                    productsLiveData.value=t
                }

            })
    }

    fun onSelectedSortChangedByUser(sort: Int) {
        this.sort = sort
        this.selectedSortLiveData.value = sortTitle[sort]
        getProducts()
    }

    fun addToFavorites(product:Product){
        if (product.isFavorite){
            productRepository.deleteFromFavorites(product).subscribeOn(Schedulers.io())
                .subscribe(object : NikeCompletableObserver(compositeDisposable){
                    override fun onComplete() {
                        product.isFavorite=false
                    }
                })
        }else{
            productRepository.addToFavorites(product).subscribeOn(Schedulers.io())
                .subscribe(object : NikeCompletableObserver(compositeDisposable){
                    override fun onComplete() {
                        product.isFavorite=true
                    }
                })
        }
    }
}