package com.parsa.nikestore.feature.product.comment

import android.os.Bundle
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.parsa.nikestore.R
import com.parsa.nikestore.common.EXTRA_ID
import com.parsa.nikestore.common.NikeActivity
import com.parsa.nikestore.data.Comment
import org.koin.androidx.viewmodel.ext.android.viewModel
import org.koin.core.parameter.parametersOf
import java.util.ArrayList

class CommentListAc : NikeActivity() {

    val commentListViewModel:CommentListViewModel by viewModel { parametersOf(intent.extras!!.getInt(
        EXTRA_ID)) }

    val commentListAdapter=CommentListAdapter(true)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_comment_list)

        val commentsRv=findViewById<RecyclerView>(R.id.commentList_rv)
        commentsRv.layoutManager=
            LinearLayoutManager(this,RecyclerView.VERTICAL,false)
        commentsRv.adapter=commentListAdapter

        commentListViewModel.progressLiveData.observe(this){
            setProgressIndicator(it)
        }

        commentListViewModel.commentLiveData.observe(this){
            commentListAdapter.comments=it as ArrayList<Comment>
        }
    }
}