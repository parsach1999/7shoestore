package com.parsa.nikestore.feature.home

import android.os.Bundle
import android.view.MenuItem
import com.google.android.material.badge.BadgeDrawable
import com.google.android.material.bottomnavigation.BottomNavigationView
import com.google.android.material.color.MaterialColors
import com.google.android.material.navigation.NavigationBarView
import com.parsa.nikestore.feature.profile.ProfileFragment
import com.parsa.nikestore.R
import com.parsa.nikestore.common.NikeActivity
import com.parsa.nikestore.data.CartItemCount
import com.parsa.nikestore.feature.cart.CartFragment
import org.greenrobot.eventbus.EventBus
import org.greenrobot.eventbus.Subscribe
import org.greenrobot.eventbus.ThreadMode
import org.koin.androidx.viewmodel.ext.android.viewModel

class MainActivity : NikeActivity() {

    lateinit var bottomNavigationView:BottomNavigationView
    val viewModel:MainViewModel by viewModel()

    override fun onCreate(savedInstanceState: Bundle?) {

        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        bottomNavigationView=findViewById(R.id.bottomNavigationMain)
        EventBus.getDefault().register(this)
        bottomNavigationView.setOnItemSelectedListener(object :NavigationBarView.OnItemSelectedListener{
            override fun onNavigationItemSelected(p0: MenuItem): Boolean {
                when(p0.itemId){
                    R.id.home1 ->{
                        supportFragmentManager.beginTransaction().setReorderingAllowed(true)
                            .replace(R.id.fragment_container_view_main, HomeFragment()).commit()
                    }

                    R.id.cart1 ->{
                        supportFragmentManager.beginTransaction().setReorderingAllowed(true)
                            .replace(R.id.fragment_container_view_main, CartFragment()).commit()
                    }

                    R.id.profile ->{
                        supportFragmentManager.beginTransaction().setReorderingAllowed(true)
                            .replace(R.id.fragment_container_view_main, ProfileFragment()).commit()
                    }
                }


                return true
            }

        })
        bottomNavigationView.selectedItemId= R.id.home1


    }
    override fun onResume() {
        super.onResume()
        viewModel.getCartItemCount()
    }

    @Subscribe(sticky = true,threadMode = ThreadMode.MAIN)
    fun onCartItemCountChangeEvent(cartItemCount: CartItemCount){
        val badge=bottomNavigationView.getOrCreateBadge(R.id.cart1)
        badge.badgeGravity=BadgeDrawable.TOP_END
        badge.backgroundColor=MaterialColors.getColor(bottomNavigationView,R.attr.colorPrimary)
        badge.number=cartItemCount.count

        badge.isVisible=cartItemCount.count>0

    }
}