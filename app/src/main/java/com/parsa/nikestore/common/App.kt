package com.parsa.nikestore.common

import android.app.Application
import android.content.SharedPreferences
import android.os.Bundle
import androidx.room.Room
import com.facebook.drawee.backends.pipeline.Fresco
import com.parsa.nikestore.data.db.AppDatabase
import com.parsa.nikestore.data.repo.*
import com.parsa.nikestore.data.repo.banner.BannerRepository
import com.parsa.nikestore.data.repo.banner.BannerRepositoryImpl
import com.parsa.nikestore.data.repo.cart.CartRepository
import com.parsa.nikestore.data.repo.cart.CartRepositoryImpl
import com.parsa.nikestore.data.repo.comment.CommentRepository
import com.parsa.nikestore.data.repo.comment.CommentRepositoryImpl
import com.parsa.nikestore.data.repo.order.OrderRepository
import com.parsa.nikestore.data.repo.order.OrderRepositoryImpl
import com.parsa.nikestore.data.repo.product.ProductRepository
import com.parsa.nikestore.data.repo.product.ProductRepositoryImpl
import com.parsa.nikestore.data.repo.source.*
import com.parsa.nikestore.data.repo.source.banner.BannerRemoteDataSource
import com.parsa.nikestore.data.repo.source.cart.CartRemoteDataSource
import com.parsa.nikestore.data.repo.source.comment.CommentRemoteDataSource
import com.parsa.nikestore.data.repo.source.order.OrderRemoteDataSource
import com.parsa.nikestore.data.repo.source.product.ProductRemoteDataSource
import com.parsa.nikestore.data.repo.source.user.UserLocalDataSource
import com.parsa.nikestore.data.repo.source.user.UserRemoteDataSource
import com.parsa.nikestore.data.repo.user.UserRepository
import com.parsa.nikestore.data.repo.user.UserRepositoryImpl
import com.parsa.nikestore.feature.auth.AuthViewModel
import com.parsa.nikestore.feature.cart.CartViewModel
import com.parsa.nikestore.feature.checkout.CheckOutViewModel
import com.parsa.nikestore.feature.favorite.FavoriteViewModel
import com.parsa.nikestore.feature.home.HomeViewModel
import com.parsa.nikestore.feature.home.MainViewModel
import com.parsa.nikestore.feature.home.ProductAdapter
import com.parsa.nikestore.feature.list.ProductListViewModel
import com.parsa.nikestore.feature.product.ProductDetailViewModel
import com.parsa.nikestore.feature.product.comment.CommentListViewModel
import com.parsa.nikestore.feature.profile.ProfileViewModel
import com.parsa.nikestore.feature.shipping.ShippingViewModel
import com.parsa.nikestore.services.ImageLoadingService
import com.parsa.nikestore.services.ImageLoadingServiceImpl
import com.parsa.nikestore.services.http.getApiService
import org.koin.android.ext.android.get
import org.koin.android.ext.koin.androidContext
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.core.context.startKoin
import org.koin.dsl.module
import org.koin.dsl.single

class App:Application() {

    override fun onCreate() {
        super.onCreate()
        supportRtl(this)
        Fresco.initialize(this)

        val myModules= module {
            single { getApiService() }
            single<ImageLoadingService> { ImageLoadingServiceImpl() }
            single { Room.databaseBuilder(this@App,AppDatabase::class.java,"db_app").build() }
            factory<ProductRepository> {
                ProductRepositoryImpl(ProductRemoteDataSource(get()),get<AppDatabase>().productDao())
            }
            factory<BannerRepository> { BannerRepositoryImpl(BannerRemoteDataSource(get())) }
            factory {(viewType:Int)-> ProductAdapter(viewType,get()) }
            factory<CommentRepository> { CommentRepositoryImpl(CommentRemoteDataSource(get())) }
            single<SharedPreferences> { this@App.getSharedPreferences("app_settings", MODE_PRIVATE) }
            factory<CartRepository> { CartRepositoryImpl(CartRemoteDataSource(get())) }
            single<UserRepository> { UserRepositoryImpl(
                UserRemoteDataSource(get()),
                UserLocalDataSource(get())
            ) }
            single { UserLocalDataSource(get()) }
            single<OrderRepository> { OrderRepositoryImpl(OrderRemoteDataSource(get())) }
            viewModel { HomeViewModel(get(),get()) }
            viewModel { (bundle:Bundle)-> ProductDetailViewModel(bundle,get(),get(),get()) }
            viewModel { (productId:Int)-> CommentListViewModel(productId,get()) }
            viewModel { (sort:Int)-> ProductListViewModel(sort,get()) }
            viewModel { AuthViewModel(get()) }
            viewModel{ CartViewModel(get()) }
            viewModel{ MainViewModel(get()) }
            viewModel{ ShippingViewModel(get()) }
            viewModel{(orderId:Int)-> CheckOutViewModel(orderId,get()) }
            viewModel { ProfileViewModel(get()) }
            viewModel { FavoriteViewModel(get()) }
        }


        startKoin {
            androidContext(this@App)
            modules(myModules)
        }

        val userRepository: UserRepository =get()
        userRepository.loadToken()
    }


}